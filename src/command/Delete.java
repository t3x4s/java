package command;

import receiver.EditorEngine;

public class Delete extends Command{

	private EditorEngine editor;

	public Delete (EditorEngine e)
	{
		editor = e;
	}

	@Override
	public void execute() {
		// TODO Auto-generated method stub
		editor.delete();
	}
}
