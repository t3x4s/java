package command;

import invoker.UserInterface;
import receiver.EditorEngine;

public class Write extends Command{

	private EditorEngine editor;
	private UserInterface UI;

	public Write(EditorEngine e, UserInterface ui)
	{
		this.editor = e;
		this.UI = ui;
		
	}

	@Override
	public void execute() {
		// TODO Auto-generated method stub
		editor.insert(UI.getChar());
		
	}
}
