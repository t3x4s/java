package command;

import receiver.EditorEngine;

public class Paste extends Command{

	private EditorEngine editor;

	public Paste (EditorEngine e)
	{
		this.editor = e;
	}
	
	@Override
	public void execute() {
		// TODO Auto-generated method stub
		editor.paste();
	}

}
