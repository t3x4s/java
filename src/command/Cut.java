package command;

import receiver.EditorEngine;

public class Cut extends Command{

	private EditorEngine editor;

	public Cut(EditorEngine e)
	{
		this.editor = e;
	}

	@Override
	public void execute() {
		// TODO Auto-generated method stub
		
		editor.cut();
	}
}
