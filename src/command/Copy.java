package command;

import invoker.UserInterface;
import receiver.EditorEngine;


public class Copy extends Command{

	private EditorEngine editor;
	private UserInterface UI;

	
	public Copy(EditorEngine e, UserInterface UI)
	{
		this.editor = e;
		this.UI = UI;
	}

	@Override
	public void execute() {
		// TODO Auto-generated method stub
		editor.copy();
	}

}
