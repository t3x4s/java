package command;

import invoker.UserInterface;
import receiver.EditorEngine;

public class Select extends Command{

	private EditorEngine editor;
	private UserInterface UI;
	
	public Select(EditorEngine e, UserInterface UI)
	{
		this.editor = e;
		this.UI = UI;
	}

	@Override
	public void execute() {
		
		editor.setSelection(UI.getSelectionStart(),UI.getSelectionEnd());
	}
}
