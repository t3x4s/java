package invoker;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import java.util.Hashtable;

import javax.swing.*;

import observer.Observer;

import receiver.EditorEngine;

import command.Command;


public class UserInterface implements Observer
{
	/**
	 * 
	 */
	@SuppressWarnings("unused")
	private static final long serialVersionUID = 1L;

	private JFrame frame;
	
	protected JTextArea textZone;
	
	private JPanel panelButtons;
		
	public Hashtable<String,Command> commands;
	
	public EditorEngine editor;
	
	
	String lastChar;
	
	public int tableauSelection[];
	
	public UserInterface(EditorEngine editor)
	{
		lastChar="";
		tableauSelection = new int[2];

		tableauSelection[0]=0;
		tableauSelection[1]=0;
		
		
		// Cr�ation de la fen�tre
		frame = new JFrame("MiniEditeur v1");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		
		// Zone de texte
		textZone = new JTextArea(20,40);
		
	
		
		//JPanel boutons
		panelButtons = new JPanel(new GridLayout(1,3));
		
		this.editor = editor;

		// les trois boutons : couper, colleret coper
		JButton cut = new JButton("Cut");
		JButton copy = new JButton("Copy");
		JButton paste = new JButton("Paste");
		
		// ajout des boutons dans leur panel
		panelButtons.add(cut);
		panelButtons.add(copy);
		panelButtons.add(paste);
		
		// Cr�ation des �couteurs pour les boutons
		ButtonListener BL = new ButtonListener();
		copy.addActionListener(BL);
		paste.addActionListener(BL);
		cut.addActionListener(BL);
		
		
		textZone.setEditable(false);
		textZone.getCaret().setVisible(true);
		
		// Cr�ation des �couteurs pour la zone de texte
		KeyListener KL = new KeyListener();
		textZone.addKeyListener(KL);
		textZone.addMouseListener(new MouseListener());
		
		
		frame.getContentPane().add(BorderLayout.CENTER,textZone);
		frame.getContentPane().add(BorderLayout.NORTH,panelButtons);
		frame.setVisible(true);
		frame.pack();
		
		textZone.requestDefaultFocus();
		textZone.requestFocus();
	}
		
	public String getSelection(){
		return textZone.getSelectedText();
	}
	
	public void setText(String text)
	{
		textZone.setText(text);
	}
	
	public String getChar() {
		return lastChar;
	}
	
	public void setCommands(Hashtable<String, Command> h){
		this.commands = h;
		this.commands.get("write").execute();
	}
	
	private class ButtonListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e) {
			
			JButton source = (JButton)e.getSource();
			
			switch (source.getText())
			{
			case "Copy" : 	if(textZone.getSelectionEnd() > 0)
							{
				
								editor.setSelection(textZone.getSelectionStart(), textZone.getSelectionEnd());
								commands.get("copy").execute();
								textZone.requestFocus();
								textZone.getCaret().setVisible(true);

							}

						break;
			case "Cut" : 
				
						if(textZone.getSelectionEnd() > 0) 
						{
							editor.setSelection(textZone.getSelectionStart(), textZone.getSelectionEnd());
							commands.get("cut").execute();
							textZone.requestFocus();
							textZone.getCaret().setVisible(true);

						}
						 
						
						break;
			case "Paste" : 
				
						commands.get("paste").execute();
						textZone.requestFocus();
						textZone.getCaret().setVisible(true);

						break;
			default : break;
			}
			
			
		}
	}

	public Hashtable<String,Command> getCommands()
	{
		return commands;
	}

	private class KeyListener implements java.awt.event.KeyListener
	{
		
		@Override
		public void keyPressed(KeyEvent e) {
			
			e.consume();
			// Enl�ve les caract�res sp�ciaux lors d'un ALt, CTRL, Shift, etc...
			if(e.getKeyCode() != e.VK_SHIFT && e.getKeyCode() != e.VK_ALT && e.getKeyCode() != e.VK_CONTROL && e.getKeyCode() != e.VK_CAPS_LOCK){
				switch(e.getKeyChar())
				{
				case KeyEvent.VK_BACK_SPACE :
					if(textZone.getSelectionStart() != textZone.getSelectionEnd()){
						if(tableauSelection[0] != tableauSelection[1]){
							editor.setSelection(textZone.getSelectionStart(),textZone.getSelectionEnd());

							tableauSelection[0] = 0;
							tableauSelection[1] = 0;
						}
					}
					commands.get("delete").execute();	

					textZone.setText(editor.getBuffer());
					textZone.setCaretPosition(editor.getCursor());

					break;
				default :
					lastChar = String.valueOf(e.getKeyChar());
					
					editor.setCursor(textZone.getCaretPosition());
					commands.get("write").execute();
					
					//Bug ? textZone.getSelectionEnd et start s'incr�mente � chaque touche
					
					break;	
				}	
				textZone.requestFocus();
				
				System.out.println("curseur text zone "+textZone.getCaretPosition());
			}
		}

		@Override
		public void keyReleased(KeyEvent arg0) {}

		@Override
		public void keyTyped(KeyEvent arg0) {}
		
	}
	
	private class MouseListener implements java.awt.event.MouseListener
	{

		@Override
		public void mouseClicked(MouseEvent e) {
			textZone.requestFocus();
			textZone.setCaretPosition(textZone.viewToModel(e.getPoint()));
			
			editor.setCursor(textZone.getCaretPosition());
			
			System.out.println("curseur text zone "+textZone.getCaretPosition());

			textZone.getCaret().setVisible(true);
		}

		@Override
		public void mouseEntered(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mousePressed(MouseEvent e) {

			textZone.setCaretPosition(textZone.viewToModel(e.getPoint()));
			tableauSelection[0]=textZone.getCaretPosition();
			editor.setCursor(textZone.getCaretPosition());

			System.out.println("curseur text zone "+textZone.getCaretPosition());


		}

		@Override
		public void mouseReleased(MouseEvent e) {
			tableauSelection[1]=textZone.getCaretPosition();
			
			editor.setCursor(textZone.getCaretPosition());
			System.out.println("curseur text zone "+textZone.getCaretPosition());

		}
		
	}
	
	
	public int getSelectionStart() {
		
		return textZone.getSelectionStart();
	}

	public int getSelectionEnd() {
		// TODO Auto-generated method stub
		return textZone.getSelectionEnd();
	}

	@Override
	public void notifyMe() {
		// TODO Auto-generated method stub
		this.textZone.setText(this.editor.getBuffer());
	}
}
