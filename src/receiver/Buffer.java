package receiver;

/**
 * @(#) Buffer.java
 */

public class Buffer
{
	
	private StringBuffer textZone;
	
	
	private int bufferCursor = 0;
	
	public Buffer(){
		
		textZone = new StringBuffer();
	}
	
	/**
	 * getBuffer
	 * @return String
	 */
	public String getBuffer()
	{
		
		return textZone.toString();
	}
	

	/**
	 * eraseBuffer
	 * 
	 * @param int start
	 * @paran int end
	 */
	public void earaseBuffer(int start, int end)
	{
		//System.out.println("erase "+start);
		textZone.delete(start, end);
	}
	
	
	public void clearBuffer()
	{
		textZone.delete(0,textZone.length());
	}
	/**
	 * insertChar
	 * 
	 * @param char c
	 */
	public void insertChar(char c)
	{
		//on ins�re � au niveau du curseur
		textZone.insert(getCursor(), c);
		setCursor(getCursor()+1);
	}
	
	/**
	 * insertString
	 * 
	 * @param String s
	 */
	public void insertString(String s)
	{
		this.textZone.insert(getCursor(), s);
		setCursor(getCursor()+s.length());
		System.out.println(getCursor());

	}
	
	/**
	 * insertStringAtPosition
	 * 
	 * @param String s
	 * @param int cursor
	 */
	public void insertStringAtPosition(String s, int cursor)
	{
		//System.out.println("chaine " + s +" buffer "+getBuffer());
		this.textZone.insert(cursor, s);
	}

	/**
	 * removeString
	 * 
	 * @param int start
	 * @param int end
	 */
	public void removeString(int start, int end)
	{
		
		if(start != end){	
			textZone.delete(start, end);
		}
	}
	
	
	/**
	 * removeChar
	 * 
	 */
	public void removeChar()
	{
		if(this.getCursor() > 0)
		{
			textZone.deleteCharAt(this.getCursor()-1);
			setCursor(getCursor()-1);	
		}
			
	}
	
	/**
	 * setCursor
	 * 
	 * @param int cursor
	 */
	public void setCursor(int cursor)
	{
		System.out.println("buffer cursor "+this.bufferCursor);
		if(cursor >= 0)
		{
			this.bufferCursor = cursor;
		}
	}
	
	/**
	 * getCursor
	 * 
	 * @return int 
	 */
	public int getCursor() {
		return bufferCursor;
	}
	
	/**
	 * readBuffer
	 * 
	 * Retourne le contenu du buffer entre un d�but et une fin de s�lection
	 * 
	 * @param int start
	 * @param int end
	 * @return String
	 */
	public String readBuffer(int start,int end)
	{

		return textZone.substring(start, end);
	}
	

}
