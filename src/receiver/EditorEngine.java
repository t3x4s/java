package receiver;

import java.util.ArrayList;
import java.util.Iterator;

import observer.Observer;
import observer.Subject;

public class EditorEngine extends Subject
{
	private Buffer buffer;
	
	private ClipBoard clipBoard;
	
	private Selection selection;
	
	public String text;
	
	boolean cut_action;
	
	public EditorEngine(){
		observers = new ArrayList<Observer>();
		selection = new Selection();
		clipBoard = new ClipBoard();
		
		buffer = new Buffer();	
		
		cut_action=false;
	}
	
	public EditorEngine(ClipBoard c, Selection s, Buffer b)
	{
		
		this.clipBoard = c;
		this.selection = s;
		this.buffer = b;
	}
	
	/**
	 * copy
	 * 
	 * copie le texte s�lectionn� dans le clipBoard
	 */
	public void copy()
	{
		clipBoard.clear();

		
		if(selection.getEnd() > 0)
		{
			clipBoard.setContent(this.getSelection());
		}
		
		
	}
	
	/**
	 * paste
	 * 
	 * ajoute au buffer le contenu du presse papier
	 */
	public void paste( )
	{
		int i = selection.getStart();
		
		if(clipBoard.getContent().length()>0)
		{
			if(selection.getEnd() == 0)
			{
				//text = text.substring(0,i) + clipBoard.getContent() + text.substring(i);
				
				
				buffer.insertStringAtPosition(clipBoard.getContent(), buffer.getCursor() );
				
				selection.setStart(i + clipBoard.getContent().length());
			}
			else
			{	
				//on supprime le contenu en s�lection !!!
				buffer.insertStringAtPosition(clipBoard.getContent(), buffer.getCursor());
			}
			
			//on remet la s�lection � 0
			setSelection(0,0);
			
			//si un cut a �t� fait
			if (cut_action == true)
			{
				clipBoard.clear();
				cut_action = false;
			}
			notifyObservers();
		}
		
	}
	
	/**
	 * cut
	 */
	public void cut( )
	{
		
		

		if(selection.getEnd() > 0)
		{
			
			clipBoard.setContent(this.getSelection());
			text = buffer.readBuffer(0,selection.getStart())
					+ buffer.readBuffer(selection.getStart(),selection.getEnd());
			
			buffer.setCursor(selection.getEnd());
			
			buffer.removeString(selection.getStart(),selection.getEnd());
			
			cut_action = true;
		}
		notifyObservers();
	}
	
	/**
	 * insert
	 * @param s
	 */
	public void insert( String s )
	{

		if(selection.getEnd() == 0)
		{			
			buffer.insertString(s);
		}
		else
		{
			
			text = buffer.readBuffer(0,buffer.getCursor()) + clipBoard.getContent()
					+ buffer.readBuffer(buffer.getCursor()+1,selection.getEnd());
			
			
			buffer.insertString(text);
			setSelection(0,0);
		}
		notifyObservers();
	}
	
	/**
	 * delete
	 * 
	 * supprimer le contenu du buffer en fonction de la s�lection
	 * 
	 */
	public void delete()
	{
		
		if(selection.getEnd() > 0)
		{System.out.println("ici");

			buffer.setCursor(Math.min(selection.getStart(), selection.getEnd()));

			buffer.removeString(selection.getStart(),selection.getEnd());
			
			//on remet la s�lection � 0
			setSelection(0,0);


			
		}
		else if(selection.getStart() > 0)
		{						
			buffer.setCursor(selection.getStart());
			
			buffer.removeString(selection.getStart(),selection.getEnd());
			
			//on remet la s�lection � 0
			selection.setStart(0);
			selection.setEnd(0);
		}
		else
		{
			buffer.removeChar();
		}
		notifyObservers();
	}
	
	/**
	 * getSelection
	 * 
	 * @return String la s�lection
	 */
	public String getSelection(){
				
		//System.out.println(buffer.getBuffer());
		if (selection.getEnd() > 0){
			return buffer.readBuffer(selection.getStart(),selection.getEnd());
		}
		else
		{
			return "";
		}
	}
	
	/**
	 * setSelection
	 * 
	 * D�finit une s�lection
	 * 
	 * @param int start
	 * @param int end
	 */
	public void setSelection(int start, int end) {
		selection.setStart(start);
		selection.setEnd(end);
		
		
	}

	
	public void setCursor(int s)
	{
		System.out.println("curseur pass� � "+s);
		buffer.setCursor(s);
	}
	
	@Override
	public void notifyObservers() {
		for (Iterator<Observer> it = observers.iterator(); it.hasNext();) {
			Observer o = it.next();
			o.notifyMe();
		}
	}

	@Override
	public void registerObserver(Observer o) {
		observers.add(o);

	}

	@Override
	public void unregisterObserver(Observer o) {
		observers.remove(o);
	}

	public String getBuffer() {
		return buffer.getBuffer();
	}

	public void setBuffer(String string) {
		buffer.clearBuffer();
		buffer.insertString(string);
		
	}

	public int getCursor() {
		return buffer.getCursor();
	}


	


}
