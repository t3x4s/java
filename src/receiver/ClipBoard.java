package receiver;

/**
 * 
 * La classe ClipBoard g�re le contenu du buffer qui sera r�cup�r�
 */
public class ClipBoard 
{

	/**
	 * Variable content
	 */
	private String content;
	
	/**
	 * Constructeur
	 */
	public ClipBoard()
	{
		this.content = ""; //initialisation � vide
	}
	
	/**
	 * getContent
	 *
	 * Retourne le contenu de la chaine
	 */
	public String getContent()
	{
		
		return content;
	}
	
	/**
	 * setContent
	 * 
	 * Modifie le contenu de la chaine
	 */
	public void setContent(String s)
	{
		content = s;
	}

	/**
	 * clear
	 * 
	 * vide le presse papiers une fois une action coller effectu�e
	 * 
	 */
	public void clear() {
		// TODO Auto-generated method stub
		
		content= "";
		
	}
}
