package receiver.test_receiver;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import receiver.ClipBoard;

public class TestClipBoard {

	ClipBoard CB;
	
	@Before
	public void init()
	{
		CB = new ClipBoard();
	}
	
	@Test
	public void test_setContent()
	{
		CB.setContent("test");
		assertEquals(CB.getContent(),"test");
		
		CB.setContent("new test");
		assertEquals(CB.getContent(),"new test");
	}
	
}
