package receiver.test_receiver;



import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import receiver.Buffer;
import receiver.ClipBoard;
import receiver.EditorEngine;
import receiver.Selection;


public class TestEditorEngine 
{
	EditorEngine e;
	
	Selection select;
	Buffer buffer;
	ClipBoard clipBoard;
	
	String text;
	String selectionText;
	
	@Before
	public void init()
	{
		
		select = new Selection();
		buffer = new Buffer();
		clipBoard = new ClipBoard();
		e = new EditorEngine(clipBoard,select,buffer);
		text = new String();
		selectionText = new String();
	}

	
	//@Test
	/*public void test_insert()
	{
		e.clipBoard.setContent("test insert");
		e.insert(e.clipBoard.getContent());
		assertEquals(e.buffer.getBuffer(),"test insert");
		
		e.clipBoard.setContent(" suite test");
		e.insert(e.clipBoard.getContent());
		assertEquals(e.buffer.getBuffer(),"test insert suite test");
	}
	
	
	@Test
	public void test_getSelection()
	{
		e.buffer.insertString("test editorEngine");
		e.selection.setEndSelection(4);
		selectionText = e.getSelection();
		
		assertEquals("test",selectionText);
	}
	
	
	@Test
	public void test_copy()
	{
		e.buffer.insertString("test editorEngine");
		e.selection.setEnd(11);
		selectionText = e.getSelection();
		
		e.copy();
		assertEquals(e.clipBoard.getContent(),"test editor");
		
	}
	
	@Test
	public void test_cut()
	{
		e.text = "test editorEngine";
		e.selection.setEnd(4);
		e.buffer.insertString(e.text);
		e.cut();
		assertEquals(e.clipBoard.getContent(),"test");
		assertEquals(e.buffer.getBuffer()," editorEngine");
	}*/
	
	@Test
	public void test_Paste()
	{
		buffer.insertString(" editorEngine");
		clipBoard.setContent("test");
		e.paste();
		assertEquals(buffer.getBuffer(),"test editorEngine");
		assertEquals(clipBoard.getContent(),"");
		buffer.clearBuffer();
		
	/*	e.clipBoard.setContent(" paste");
		e.selection.setEnd(e.buffer.getBuffer().length());
		e.paste();
		assertEquals(e.buffer.getBuffer(),"test paste editorEngine");*/
	}
}
