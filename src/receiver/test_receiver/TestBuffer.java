package receiver.test_receiver;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import receiver.Buffer;


public class TestBuffer {

	
	private Buffer b;
	private StringBuffer sb;
	
	@Before
	public void init()
	{
		b = new Buffer();
		sb = new StringBuffer();
		sb.append("Test du buffer");
	}
	
	
	/*@Test
	public void test_insertChar()
	{
		
		b.insertChar(sb.charAt(0));
		assertEquals("Premier caract�re",'T',sb.charAt(0));
		
		b.insertChar(sb.charAt(1));
		assertEquals(b.getBuffer(),"Te");
	}
	
	@Test
	public void test_insertString()
	{
		b.insertString("test");
		assertEquals(b.getBuffer(),"test");
		
		b.insertString("suite");
		assertEquals(b.getBuffer(),"testsuite");
	}
	
	@Test
	public void test_insertStringAtPosition()
	{
		b.insertString("test");
		b.insertStringAtPosition("start ", 0);
		assertEquals(b.getBuffer(),"start test");
		
		b.insertStringAtPosition(" end ", 6);
		assertEquals(b.getBuffer(),"start  end test");
	}
	
	@Test
	public void test_removeString()
	{
		b.insertString("test");
		b.removeString(0, 2);
		assertEquals(b.getBuffer(),"st");
		
		b.removeString(0, 5);
		assertEquals(b.getBuffer(),"");
	}*/
	
	@Test
	public void test_setCursor()
	{
		b.setCursor(10);
		assertEquals(b.getCursor(),10);
		
		b.setCursor(-1);
		assertEquals(b.getCursor(),10);
	}
}
