package receiver.test_receiver;

import static org.junit.Assert.*;



import receiver.Selection;

import org.junit.Before;
import org.junit.Test;



public class TestSelection 
{
	private Selection s;
	StringBuilder sb = new StringBuilder();
	
	
	@Before
	public void init()
	{
		sb.append("Test de selection");
		s = new Selection(0,sb.length());
	}
	
	/**
	 * Retourne le charact�re de d�but de chaine
	 */
	@Test
	public void test_getStart()
	{
		assertEquals("La position de T",0,s.getStart()); 
	}
	
	@Test
	public void test_getEnd()
	{
		assertEquals("Position de n",sb.length(),s.getEnd());
	}
	
	@Test
	public void test_setStart()
	{
		s.setStart(0);
		assertEquals("S�lection � 0",0,s.getStart());
		
		s.setStart(-1);
		assertEquals("S�lectio n�gative non pris en compte",0,s.getStart());
		
	}
	
	@Test
	public void test_setEnd()
	{
		s.setEnd(5);
		assertEquals("S�lection de 0 � 5",5,s.getEnd());
		
		s.setEnd(-1);
		assertEquals("S�lection n�gative non prise en compte",5,s.getEnd());
	}
	
	@Test
	public void test_setEndSelection()
	{
		s.setEndSelection(3);
		assertEquals("Selection de 0 � 3",3,s.getEnd());
		
		s.setStart(10);
		s.setEndSelection(3);
		assertEquals("Selection end > start",3,s.getStart());
		assertEquals(10,s.getEnd());
	}
	
}

