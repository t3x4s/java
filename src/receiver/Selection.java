package receiver;

/**
 * 
 * La classe s�lection permet d'avoir les �lements d'une s�lection 
 * c'est � dire le d�but et la fin de la s�lection
 *
 */
public class Selection
{

	/**
	 * Variable d�but de s�lection
	 */
	private int start;
	
	/**
	 * Variable fin de s�lection
	 */
	private int end;
	

	
	/**
	 * Constructeur sans param�tres
	 */
	public Selection(){}
	
	/**
	 * Constructeur
	 * 
	 * @param int start
	 * @param int end
	 */
	public Selection(int start, int end)
	{
		this.start = start;
		this.end = end;
	}
	
	
	/**
	 * getStart
	 * Retourne la valeur de d�but de s�lection 
	 * 
	 * @return int start
	 */
	public int getStart()
	{
		return start;
	}
	
	/**
	 * getEnd
	 * 
	 * Retourne la fin de la s�lection
	 * 
	 * @return int end
	 */
	public int getEnd()
	{
		return end;
	}
	
	
	/**
	 * setStart
	 * 
	 * Modifie le d�but de la s�lection
	 * 
	 * @param start
	 */
	public void setStart(int start)
	{
		if(start >= 0) this.start = start;
	}
	
	
	/**
	 * setEnd
	 * 
	 * Modifie la fin de la s�lection
	 * 
	 * @param end
	 */
	public void setEnd(int end)
	{
		if(end >= 0) this.end = end;
	}
	
	
	/**
	 * setEndSelection
	 * 
	 * Modifie la fin d'une s�lection si l'utilisateur s�lectionne de droite � gauche
	 * NB : end > 0
	 * 
	 * @param int end
	 */
	public void setEndSelection(int end)
	{
		if (this.start > end)
		{
			int tmp = this.start; //variable temporaire
			this.start = end;
			this.end = tmp;
		}
		else this.end = end;
	}
	


}

