package main;

import java.util.Hashtable;

//import observer.IHMObserver;

import command.*;


import receiver.EditorEngine;
import invoker.UserInterface;

public class main {

	private static UserInterface UI;
	private static EditorEngine editor;
	private static Hashtable<String,Command> commands;
	
	public static void main(String[] args){
		
		
		editor = new EditorEngine();
		
		UserInterface UI = new UserInterface(editor);

		commands = new Hashtable<String,Command>();	
		commands.put("cut", new Cut(editor));
		commands.put("copy", new Copy(editor, UI));
		commands.put("paste", new Paste(editor));
		commands.put("select", new Select(editor,UI));
		commands.put("write", new Write(editor,UI));
		commands.put("delete", new Delete(editor));

		
		UI.setCommands(commands);
		editor.registerObserver(UI);
	}

}
